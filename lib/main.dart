import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:utils/tab_bloc.dart';
import 'switch_bloc.dart';
void main() {
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,

      home:Scaffold(
        body:
            Column(
              children: <Widget>
              [
              SizedBox(
                height: 50,
              ),
                BlocProvider(
                  create: (BuildContext context) => TabBloc(true),
                  child : Home1(),

                ),
            BlocProvider(
            create: (BuildContext context) => SwitchBloc(false),
      child : Home(),
            ),


              ],
            )



      )


    );
  }
}

class Home extends StatelessWidget {
  bool isSwitched = false;

  @override
  Widget build(BuildContext context) {
    final _bloc = BlocProvider.of<SwitchBloc>(context);

    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[



          BlocBuilder<SwitchBloc, bool>(

            builder: (BuildContext context, bool state){
              isSwitched=state;
              return   Switch(
                value: isSwitched,
                onChanged: (value){
                  if(value==false)
                    _bloc.add(SwitchEvents.off);
                      else
                    _bloc.add(SwitchEvents.on);
                      isSwitched=state;
                },
                activeTrackColor: Colors.lightGreenAccent,
                activeColor: Colors.green,
              );
            },
          ),



        ],
      ),
    );
  }

}

class Home1 extends StatelessWidget {
  bool toggle;
  @override
  Widget build(BuildContext context) {
    final _tabbloc = BlocProvider.of<TabBloc>(context);
      return
        BlocBuilder<TabBloc, bool>(
            builder: (BuildContext context, bool state){
              toggle=state;
              return
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[

                      Container(
                        width: MediaQuery.of(context).size.width/2,
                        height: 30,
                        decoration: BoxDecoration(
                            color: toggle==true?Colors.blue:Colors.grey[100],
                            border:Border(bottom: BorderSide(color: Colors.grey,width: 1,style: BorderStyle.solid))
                        ),
                        child:  FlatButton(
                            onPressed: ()
                        {
                          _tabbloc.add(TabEvents.on);
                          toggle=state;
                          print(state);
                        },
                            child: Text("Toggle")
                        ),
                      ),

                     Container(
                       decoration: BoxDecoration(
                         color: toggle==false?Colors.blue:Colors.grey[100],
                         border:Border(bottom: BorderSide(color: Colors.grey,width: 1,style: BorderStyle.solid))
                       ),
                         width: MediaQuery.of(context).size.width/2,
                         height: 30,
                        // color: toggle==false?Colors.blue:Colors.grey,
                       child: FlatButton(onPressed: ()
                       {
                         _tabbloc.add(TabEvents.off);
                         toggle=state;
                         print(state);
                       },
                           child: Text("Not Toggle")
                       )
                     )

                    ],
                  );
            },
          );
  }

}
