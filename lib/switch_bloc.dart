import 'package:flutter_bloc/flutter_bloc.dart';

enum SwitchEvents {on, off}

class SwitchBloc extends Bloc<SwitchEvents, bool>{
  SwitchBloc(bool initialState) : super(initialState);



  @override
  // TODO: implement initialState
  bool get initialState => false;

  @override
  Stream<bool> mapEventToState(SwitchEvents event) async*{
    // TODO: implement mapEventToState
    switch(event){
      case SwitchEvents.on:
        yield true;
        break;
      case SwitchEvents.off:
        yield false;
        break;
    }
  }

}