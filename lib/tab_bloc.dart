import 'package:flutter_bloc/flutter_bloc.dart';

enum TabEvents {on, off}

class TabBloc extends Bloc<TabEvents, bool> {
  TabBloc(bool initialState) : super(initialState);


  @override
  // TODO: implement initialState
  bool get initialState => true;

  @override
  Stream<bool> mapEventToState(TabEvents event) async* {
    // TODO: implement mapEventToState
    switch (event) {
      case TabEvents.on:
        yield true;
        break;
      case TabEvents.off:
        yield false;
        break;
    }
  }
}